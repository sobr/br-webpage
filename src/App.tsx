import React, {useEffect, useRef, useState} from 'react';
import "./App.sass"
import {ReactComponent as BgMobileSVG} from "./assets/bg_stars_mobile.svg";
import {ReactComponent as BgDesktopSVG} from "./assets/bg_stars_desktop.svg";
import {UnicornClicker} from "./unicorn-clicker";


interface Star {
    el: SVGElement;
    d: number;
    a: number;
    s: number;
}


function App() {
    const [time, setTime] = useState<string>("00:00:00");
    const [isLabelShow, setLabelShow] = useState<boolean>(false);
    const [isMobile, setIsMobile] = useState<boolean>(true);
    const [isGlowShow, setGlowShow] = useState<boolean>(false);
    const [isGlowAnimated, setGlowAnimated] = useState<boolean>(false);
    const containerBgRef = useRef<SVGSVGElement>(null);
    const counterRef = useRef<HTMLDivElement>(null);
    const unicornRef = useRef<HTMLDivElement>(null);
    const starRef = useRef<HTMLDivElement>(null);

    const p2 = (v: number) => {
        v>>=0;
        if (v>=10) return v.toString();
        return `0${v}`;
    }

    const setLabelPosition = () => {
        if (!counterRef.current) return;
        const el = counterRef.current;
        el.style.left = `${document.body.offsetWidth/2 - el.offsetWidth*.98/2}px`;
    };

    const updateTime = () => {
        const targetTime = new Date('2021-09-04T23:00:00+02:00');
        const currentTime = new Date();
        const secs = (targetTime.getTime()-currentTime.getTime())/1000>>0;
        const hours = p2(secs/3600);
        const minutes = p2(secs%3600/60);
        const seconds = p2(secs%60);
        setTime(`${hours}:${minutes}:${seconds}`);
    };

    const moveStars = (stars: Star[]) => {
        const el = containerBgRef.current;
        if (!el) return;

        for (const s of stars) {
            s.a += s.s * (30/1000);
            const x = Math.sin(s.a)*s.d;
            s.el.style.transform = `translateY(${x}%)`;
        }
    };

    const collectStars = (): Star[] => {
        const el = containerBgRef.current;
        if (!el) return [];
        return Array.from(el.getElementsByClassName("star")).map(starEl => ({
            el: starEl as SVGElement,
            d: Math.random()+0.5,
            a: Math.random()>.5?(2*Math.PI):0,
            s: Math.random()*0.8+0.2
        }));
    };

    const showGlow = () => {
        setGlowShow(true);
        setTimeout(()=>setGlowAnimated(true), 1000);
    };

    useEffect(()=>{
        if (!starRef.current || !unicornRef.current) return;
        if (isMobile && document.body.offsetWidth>=480) {
            setIsMobile(false);
            return;
        }
        updateTime();
        setLabelPosition();
        const to = setInterval(()=>{
            updateTime();
        }, 1000);
        const resize = () => {
            setLabelPosition();
        }
        window.addEventListener("resize", resize);
        setLabelShow(true);
        let isMoving = true;

        const stars = collectStars();

        const move = () => {
            if (!isMoving) return;
            moveStars(stars);
            window.requestAnimationFrame(()=>move());
        };

        move();

        return () => {
            clearTimeout(to);
            window.removeEventListener("resize", resize);
            isMoving = false;
        }
    }, [isMobile]);

    return (
        <div className="container">
            <div className="container-bg">
                {isMobile&&<BgMobileSVG ref={containerBgRef}/>}
                {!isMobile&&<BgDesktopSVG ref={containerBgRef}/>}
            </div>
            <div className="logo-star" ref={starRef}/>
            <div className="logo-type"/>
            <div className={"logo-tagline "+(isLabelShow?'show':'')}/>
            <div className="logo-line"/>
            <div className="next-update-label">
                next update in
            </div>
            <div className="counter-label" ref={counterRef}>
                {time}
            </div>
            <div className="social-icons">
                <a className="icon discord" href="https://discord.gg/HVeaMfcJxM"/>
                <a className="icon twitter" href="https://twitter.com/BoredRockstarz"/>
            </div>
            <div className="unicorn" ref={unicornRef}>
                {/*<div className={"glow "+(isGlowShow?'show':'')+" "+(isGlowAnimated?'animated':'')} />*/}
            </div>
        </div>
    );
}

export default App;
