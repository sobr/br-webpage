import "./Faq.sass";
import {useState} from "react";

interface FaqItem {
    header: string;
    content: string;
}

const FaqItems: FaqItem[] = [
    {
        header: "What is Böred Rockstarz?",
        content: "We're an original collection of 10'000 randomly assembled Rockstarz hanging out on the ETH blockchain as ERC721 Tokens (NFT). We are fucking cute btw! Stay tuned..."
    },
    {
        header: "So you're just ripping off the apes then??",
        content: "Sigh, here we go again, another Ripoff of our beloved Apes? After the Mummies, the Bananas and everything else you're now jumping on the Bandwagon with your fucking Rockstarz?<br/><br/>We hear ya!<br/><br/>Do we care though? Nah, not really. And here's why:<br/><ul><li>we're fucking Rockstarz!</li><li>After 2 fucking years without touring, there simply is no better word to use for our current condition. We are bored as fuck. Simple as that!</li><li>name just one rockstar who has not used samples or covered a song during his career... (we know, that's actually fucking hard 😉)</li><li>Music and all art has always been building on what was done before. That's what it's meant to do: inspire other Artists to go an do great things.</li></ul>"
    },
    {
        header: "What's up with the Special-Character-Thingy in your name?",
        content: "It makes us feel really special. Don't ask us why, it just does. If you really want to know more about this topic, check out the <a target=\"_blank\" href=\"https://en.wikipedia.org/wiki/Metal_umlaut\">metal umlaut.</a>)"
    },
    {
        header: "WTF Unicorns?!?",
        content: "WTF, Human?!? Now go to your room and think about how that makes you feel! 🎙️💧"
    },
    {
        header: "When is launch?",
        content: "Aiming for launch for mid September. Final launch date not fixed yet."
    },
    {
        header: "What's an NFT?",
        content: "Seriously dude? Be sure to tell us how and why you ended up here. Also, you might want to check this link: <a target=\"_blank\" href=\"https://lmgtfy.app/?q=what+is+nft\">What is an NFT?</a> . We still ❤️ you though... if you need any help, you can of course always reach out to us via twitter/discord/email..."
    },
    {
        header: "Where's your contract? ",
        content: "We'll link the contract here once we have the final version up & verified."
    },
    {
        header: "Where can I trade the Böred Rockstarz?",
        content: "We'll put up the link to the official Collection on Opensea as soon as we are live. Please don't buy any Rockstarz before that anywhere."
    },
    {
        header: "So are you guys the real Deal? Like actual, real life fucking Rockstars?",
        content: "In our wildest dreams, yes. Sorry to disappoint you, dude. We actually are a bunch of music loving crypto nerds and artists. <br/>" +
            "Who were you expecting here, though? Mick Jagger? Kanye West? Dave motherfucking Grohl? 😉"
    },
    {
        header: "How much?",
        content: "Maybe <a target=\"_blank\" href=\"https://www.youtube.com/watch?v=cbB3iGRHtqAIt%27ll\">they</a> know:  <br/>" +
            "It’ll cost you 0.0666 ETH (+gas) to book 1 Bored Rockstar. If our lawyer wasn't breathing down our neck we would tell you that this is the greatest investment 💰  ever. <br/>" +
            "But please: do your own research and make an informed decision..."
    },
    {
        header: "What can I do with my Rockstar?",
        content: "It will give you a voice in our band. So you can have a say on what our first musical creation will sound like and how we spend the community funds. As a holder you will also participate in any community events. <br/>" +
            "Other than that you can do anything with it. Like, whatever you want. Since they are unicorns (for some weird reason ^^) you can also dress them up as ponies for your kids next birthday party. The children will have a blast riding around on them 🎠. Just make sure to keep 'em away from the missus…<br/>"
    }
]

export function Faq() {
    const [active, setActive] = useState<string>(FaqItems[0].header);

    return <div className="section-faq">
        <div className="section-header" />
        <div className="faq-container">
            {FaqItems.map(f => <div className={"faq-item " + (f.header==active?'active':'')} key={f.header}>
               <div className="faq-header" onClick={()=>{setActive(f.header)}}>
                   <div className="header-text" dangerouslySetInnerHTML={{__html:f.header}} />
                   <div className="arrow" />
               </div>
               <div className="faq-content" dangerouslySetInnerHTML={{__html:f.content}} />
            </div>)}
        </div>
    </div>;
}