import "./RoadMap.sass";
import {useEffect, useRef} from "react";

export function RoadMap() {
    const blocksRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (!blocksRef.current || document.body.offsetWidth < 480) return;
        const blocks = blocksRef.current;
        const onScroll = () => {
            const scrollTop = window.scrollY;
            // @ts-ignore
            blocks.querySelectorAll('.block').forEach((block: HTMLElement) => {
                if (block.getBoundingClientRect().top < window.innerHeight*.9) {
                    if (!block.className.includes(' show')) {
                        block.className += ' show';
                    }
                }
            });
        };
        onScroll();
        document.addEventListener('scroll', onScroll);
        return () => {
            document.removeEventListener('scroll', onScroll);
        }
    }, []);

    return <div className="section-roadmap">
        <div className="section-header"/>
        <div className="roadmap-blocks" ref={blocksRef}>
            <div className="block left">
                <div className="icon rockstar"/>
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">0%</div>
                        We're gonna <a target="_blank" href="https://www.youtube.com/watch?v=G2-fqdCKCMA">create a new
                        music album</a> now! That's what we're here for, right?
                    </div>
                    <div className="bubble"/>
                </div>
            </div>
            <div className="block right">
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">25% - Airdrop time!</div>
                        Dude, you don't get it. You can't just go straight to business! We have to keep our crowd
                        entertained. <br/>Let's have 10 of our Bored Rockstarz do a fucking stage dive straight into the
                        accounts of 10 existing holders.
                    </div>
                    <div className="bubble"/>
                </div>
                <div className="icon nerd"/>
            </div>

            <div className="block left">
                <div className="icon rockstar"/>
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">66.6% - ETH Raffle</div>
                        Ok, crypto nerd, you're right. But holy shit! We are at <a target="_blank"
                                                                                   href="https://www.youtube.com/watch?v=WxnN05vOuSM">66.6%</a> already!<br/> So
                        let's put some ETH back into random wallets of our fans! <br/><br/>Did I do this Raffle thing
                        right?
                    </div>
                    <div className="bubble"/>
                </div>
            </div>
            <div className="block right">
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">75% - Airdrop II</div>
                        See, that wasn't so hard now, was it? You're starting to <a target="_blank"
                                                                                    href="https://www.youtube.com/watch?v=LBw5FtCe430">catch
                        my drift</a>.<br/><br/>But now it's time for an even more glorious stage dive. This time the
                        Rockstarz shall bring some friends along.
                    </div>
                    <div className="bubble"/>
                </div>
                <div className="icon nerd"/>
            </div>


            <div className="block left">
                <div className="icon random-rockstar"/>
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">90%</div>
                        Nothing special happening here. Just wanted to start the <a target="_blank"
                                                                                    href="https://www.youtube.com/watch?v=9jK-NcRmVcw">Final
                        countdown</a>. Now <a href="https://www.youtube.com/watch?v=2X_2IdybTV0">carry on, sons</a>...
                    </div>
                    <div className="bubble"/>
                </div>
            </div>
            <div className="block right">
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">100%</div>
                        The Band is complete. Time to get to <a target="_blank"
                                                                href="https://www.youtube.com/watch?v=pt8VYOfr8To">work
                        bitch</a>!
                    </div>
                    <div className="bubble"/>
                </div>
                <div className="icon nerd"/>
            </div>

            <div className="block left">
                <div className="icon rockstar"/>
                <div className="text-wrapper">
                    <div className="text">
                        <div className="text-header">
                            Finally!
                        </div>
                        Now let's create some fucking music! We're gonna create the <a target="_blank"
                                                                                       href="https://www.youtube.com/watch?v=_lK4cX5xGiQ">best
                        album in the world</a>!
                    </div>
                    <div className="bubble"/>
                </div>
            </div>
        </div>
    </div>
}
