import {Scene} from "./Scene";
import "./Header.sass";
import twitter from "../assets/Main_Landing_Page/Website_Assets/icon-twitter.svg";
import opensea from "../assets/Main_Landing_Page/Website_Assets/icon-opensea.svg";
import discord from "../assets/Main_Landing_Page/Website_Assets/icon-discord.svg";
import {Menu} from "../Page";

import {config} from "../config";
import {useState} from "react";
import {SceneMobile} from "./SceneMobile";

export function Header({
    onNavigation = (menu: Menu)=>{},
    activeNavigation = "about",
    isMobile = true,
}: {
    onNavigation: (menu: Menu)=>void,
    activeNavigation: Menu,
    isMobile: boolean
}) {
    const [menuOpen, setMenuOpen] = useState<boolean>(false);

    const onNav = (nav: Menu) => {
        onNavigation(nav);
        setMenuOpen(false);
    }

    return <div className="section-head">
        <div className="header">
            <div className="logo"/>
            <div className="menu-trigger" onClick={()=>setMenuOpen(v=>!v)}/>
            <div className={(isMobile?"mobile-menu":"menu")+" "+(menuOpen?"open":"")}>
                <ul>
                    {
                        ([
                            ["about", "About"],
                            ["roadmap", "Roadmap"],
                            ["worldtour", "Worldtour"],
                            ["faq", "FAQ"],
                            ["team", "Team"],
                        ] as [Menu, string][]).map(([link, name]) =>
                            <li key={link}>
                                <a href=""
                                   className={activeNavigation == link ? "active" : ""}
                                   onClick={e=>{onNav(link);e.preventDefault()}}
                                >{name}</a>
                            </li>
                        )
                    }
                </ul>
            </div>
            <div className="icons">
                <ul>
                    <li>
                        <a href={config.openseaLink}>
                            <img src={opensea} />
                        </a>
                    </li>
                    <li>
                        <a href={config.discordLink}>
                            <img src={discord} />
                        </a>
                    </li>
                    <li>
                        <a href={config.twitterLink}>
                            <img src={twitter} />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div className="scroll-down" onClick={()=>onNav("about")}/>
        {isMobile&&<SceneMobile />}
        {!isMobile&&<Scene />}
    </div>;
}
