import "./Team.sass";

export function Team() {
    return <div className="section-team">
        <div className="section-header" />
        <div className="team-container">
            <div className="item">
                <div className="icon r1"><div /></div>
                <div className="content">
                    <div className="header">Fairy Mercury</div>
                    <div>
                        <p className="role">Lead Singer, Creative Dev Dude</p>
                        <p className="description">
                            Eternally inspired by the one time in the karaoke club
                            when not everyone straight up left the moment he started singing...
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=6k7esY7g4Y8">will you do the Fandango?</a>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="icon r2"><div /></div>
                <div className="content">
                    <div className="header">UniKoЯn</div>
                    <div>
                        <p className="role">Wannabe Drummer, Creative Dev Dude</p>
                        <p className="description">
                            Oh, and also:<br/>
                            Boom na da noom na na nema
                            <br/>Da boom na da noom na namena
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=jRGrNDV2mKc">Go!</a>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="icon r3"><div /></div>
                <div className="content">
                    <div className="header">"Fuck Sake" Steve</div>
                    <div>
                        <p className="role">Lead Guitarist, currently MIA</p>
                        <p className="description">
                            Current whereabouts: unknown.<br/>
                            Help us find Steve over in our  <a target="_blank" href="https://discord.gg/HVeaMfcJxM">discord!</a>
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=aXqkNF8fgs8&t=23s">Steve! Steve!</a>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="icon r4"><div /></div>
                <div className="content">
                    <div className="header">Creative Bourbon</div>
                    <div>
                        <p className="role">Roadie</p>
                        <p className="description">
                            Keeping things running smootlh as hell
                            behind the scenes.
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=8y7105u0-94">We are the road crew</a>
                    </div>
                </div>
            </div>
            <div className="item">
                <div className="icon r5"><div /></div>
                <div className="content">
                    <div className="header">Corny Horn/Horny Corn</div>
                    <div>
                        <p className="role">Bassist, Artist</p>
                        <p className="description">
                            Where are my groupies?<br/>
                            Wanna have some fun? Me too!
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=-j6muYHRX90">Have fun</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
