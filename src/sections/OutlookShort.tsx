import amazonmusic from "./../assets/Main_Landing_Page/Website_Assets/logo-amazon-music.svg";
import spotify from "./../assets/Main_Landing_Page/Website_Assets/logo-spotify.svg";
import applemusic from "./../assets/Main_Landing_Page/Website_Assets/logo-apple-music.svg";
import "./Outlook.sass";


export function OutlookShort() {
    return <div className="section-outlook">
            <div className="music-block short">
                <div className="music-block-content">
                    <div className="left">
                        <div className="header">Böred Rockstarz music album</div>
                        <div className="comment">
                            <p>
                            As a Böred Rockstarz owner, you will be part of the community that creates a first of it’s
                            kind music album!<br/>
                            We have some really dope musicians* ready to start working on the tracks. They will do all the heavy
                            lifting. But they need inspiration!<br/><br/>
                            So all of you freaking good looking and inspiring bunch of Böred Rockstarz will be their muses!
                            </p>
                            <p className="annotation">* If you know or even are a talented musician: get in touch! We always have room for guest stars!</p>
                        </div>
                    </div>
                    <div className="right">
                        <div className="list">
                            <ul>
                                <li><span>10 original songs and accompanying artwork, released as NFTs</span></li>
                                <li><span>the songs will be auctioned off</span></li>
                                <li><span>revenue will be split between community and charity of the artists choice</span></li>
                                <li><span>(and yes: we’ll work out the legal stuff with the song buyers)</span></li>
                            </ul>
                        </div>
                        <div className="avail-comment">
                            available soon on
                        </div>
                        <div className="services">
                            <div className="service amazon">
                                <img src={amazonmusic}/>
                            </div>
                            <div className="service spotify">
                                <img src={spotify}/>
                            </div>
                            <div className="service apple">
                                <img src={applemusic}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
}
