import opensea from "../assets/Main_Landing_Page/Website_Assets/icon-opensea.svg";
import discord from "../assets/Main_Landing_Page/Website_Assets/icon-discord.svg";
import twitter from "../assets/Main_Landing_Page/Website_Assets/icon-twitter.svg";
import "./Footer.sass";

import {config} from "../config";

export function Footer() {
    return <div className="footer-section">
        <div className="logo" />
        <div className="icons">
            <ul>
                <li>
                    <a href={config.openseaLink}>
                        <img src={opensea} />
                    </a>
                </li>
                <li>
                    <a href={config.discordLink}>
                        <img src={discord} />
                    </a>
                </li>
                <li>
                    <a href={config.twitterLink}>
                        <img src={twitter} />
                    </a>
                </li>
            </ul>
        </div>
    </div>
}
