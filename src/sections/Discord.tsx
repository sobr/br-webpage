import "./Discord.sass";
import {config} from "../config";

export function Discord() {
    return <div className="section-discord">
        <div className="discord-block">
            <div className="unicorn"/>
            <div className="text">
                Join the fun and <br/>
                become part of our <br/>
                community on Discord

                <div className="button">
                    <a href={config.discordLink}>
                        <div>DISCORD</div>
                        <div className="arrow"/>
                    </a>
                </div>
            </div>
        </div>
    </div>;
}
