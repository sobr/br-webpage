import "./Worldtour.sass";
import {useEffect, useRef, useState} from "react";

export function Worldtour({isMobile = true}: { isMobile: boolean }) {
    const [posterShow, setPosterShow] = useState(false);
    const posterRef = useRef<HTMLDivElement>(null);
    const posterClick = () => {
        if (isMobile) return;
        setPosterShow(true);
    };
    const busRef = useRef<HTMLDivElement>(null);
    useEffect(()=>{
        const onKeyup = (ev: KeyboardEvent) => {
            if (ev.code == "Escape") {
                setPosterShow(false);
            }
        };
        const onClick = (ev: MouseEvent) => {
            if (isMobile) return;
            if (ev.target != posterRef.current) {
                setPosterShow(false);
            }
        };
        const onScroll = () => {
            const bus = busRef.current;
            if (!bus) return;
            const progress = 1-bus.getBoundingClientRect().top/window.innerHeight;
            const left = progress*100 - 25;
            bus.style.left = `${left}vw`;
        };
        document.addEventListener("keyup", onKeyup);
        document.addEventListener("click", onClick);
        document.addEventListener("scroll", onScroll);
        return () => {
            document.removeEventListener("keyup", onKeyup);
            document.removeEventListener("click", onClick);
            document.removeEventListener("scroll", onScroll);
        };
    }, [isMobile]);

    return <div className="section-worldtour">
        <div className="worldtour-content-wrapper">
            <div className="worldtour-content">
                <div className="worldtour-section-header"/>
                <div className="description-block">
                    <div className="header">Next up: World Tour!</div>
                    <div className="comment">
                        After the initial gig sells out, the Böred Rockstarz will be overwhelmed and completely blown
                        away from
                        all the support and love they receive from the community. <br/>
                        So they'll finally manage to get their shit together and get back to work.<br/><br/>
                        So what’s next in the world of the BOROs?
                        Well, quite obviously it’s time for a fucking World Tour!<br/><br/>
                        <a target="_blank" href="https://www.bichelsee-balterswil.ch/">Bichelsee-Balterswil</a>, we're
                        coming!
                    </div>
                </div>
                <div className={"poster " + (posterShow?"show":"")} onClick={posterClick} ref={posterRef}/>
                <div className="worldtour-roadmap">
                    <div className="block left intro">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Recording Party</div>
                            Before we can start our world tour, we have to release our music album! <br/><br/>
                            So let’s head over to an awesome Studio together with our artists to finally record some
                            music!<br/><br/>
                            Let's go platinum baby! 💿 <br/><br/>
                            And while we're recording, the roadies will be keeping the floor around our stage clean.
                        </div>
                    </div>
                    <div className="block right first-stop">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Newcomer Stage</div>
                            The BOROs need supporting acts. And the world needs music. <br/>
                            We’ll set up a dedicated stage for Newcomers & Underground performers to present themselves
                            and to receive the full support from the awesome BORO community.<br/><br/>
                            For this purpose, we'll launch a community fund and start it off with 10ETH. Additionally,
                            30% of royalties on secondary sales will go into the community fund.
                            This will make sure we can keep the Newcomer Stage running forever and we all can keep on
                            rocking! 🤘<br/><br/>
                            (Even if the original Böred Rockstarz inevitably will be getting to old for this shit!)
                        </div>
                    </div>
                    <div className="block left second-stop">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Start your band</div>
                            The Böred Rockstarz come together to form different bands. <br/><br/>
                            Because, duh, no bands -&gt; no World Tour. <br/>No World Tour -&gt; no groupies. <br/>No
                            groupies -&gt; fucking boring tour life...<br/><br/>
                            As a BORO Hodler you'll get access to the bandroom.
                            This will be the place to form your band, give it an awesome name and create sick band
                            logos.<br/>

                            Once the bands have formed we'll have an awesome competition running where you can win ETH
                            and other shiny things. <br/><br/>
                            You'll also be supporting an awesome music project along the way.
                        </div>
                    </div>
                    <div className="block right third-stop">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Guest Stars</div>
                            We’ll invite Guest stars to jam and go on tour with us. Stay tuned for awesome
                            collaborations!
                        </div>
                    </div>
                    <div className="block left fourth-stop">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Groupies!</div>
                            How can we truly ever be Rockstars without Groupies? <br/><br/>
                            So it's about time for the Groupies to come and party with the Böred Rockstarz!
                        </div>
                    </div>
                    <div className="block right outro">
                        <div className="img"/>
                        <div className="text">
                            <div className="text-header">Rockin All Over The World</div>
                            What' s up next? <br/>
                            What we know for sure: we are in this for the long run. <br/>
                            There are so many possibilities where we could go next with the Böred Rockstarz and their
                            Groupies.
                            We'll keep coming up with fun ideas together with our community to keep ourselves
                            entertained and to keep adding utility and value for our Hodlers.<br/><br/>
                            Make sure to join our <a href="https://discord.gg/HVeaMfcJxM" target="_blank">Discord</a> to
                            get in on the action!<br/><br/>
                            <a href="https://www.youtube.com/watch?v=Yw_sJifUYgM" target="_blank">

                                Here we are<br/>
                                Here we are and here we go<br/>
                                All aboard<br/>
                                And we're hittin' the road<br/>
                                Here we go rockin' all over the world!<br/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="bus-wrapper">
            <div className="bus" ref={busRef}/>
        </div>
    </div>;
}
