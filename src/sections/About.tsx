import "./About.sass";


export function About() {
    return <div className="section-about">
        <div className="top-bg"/>
        <div className="about-content">
            <div className="section-header"/>
            <div className="text">
                <div className="text-header">Join our band!</div>
                <p>After two fucking years without touring we are bored as fuck! No gigs, no fans, no nothing!</p>
                <p>We need your support to get our asses up, put aside our whisky bottles and get back on stage!</p>
                <p>Together we will create the first Böred Rockstarz music album.</p>
                <p>This will be the first NFT community driven music project. And that is only the beginning of our journey...
                    <br/><br/>
                Come rock with us! 🤘</p>
            </div>
            <div className="sketch-scene"/>
            <div className="blocks">
                <div className="block ppl">
                    <div className="icon"/>
                    <div className="block-header">
                        10.000
                    </div>
                    <div className="block-comment">
                        randomly assembled<br/>
                        Bored Rockstarz
                    </div>
                </div>
                <div className="block eth">
                    <div className="icon"/>
                    <div className="block-header">
                        0.0666
                    </div>
                    <div className="block-comment">
                        ETH Flat price –<br/>
                        fair minting
                    </div>
                </div>
                <div className="block rights">
                    <div className="icon"/>
                    <div className="block-header">
                        Rights
                    </div>
                    <div className="block-comment">
                        Do whatever you want<br/>
                        with your Rockstar
                    </div>
                </div>
                <div className="block chart">
                    <div className="icon"/>
                    <div className="block-header">
                        1
                    </div>
                    <div className="block-comment">
                        Sick music album!
                    </div>
                </div>
            </div>
        </div>
    </div>
}

