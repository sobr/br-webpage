import "./Icons.sass";

export function Icons() {
    return <div className="section-icons">
        <div className="icon gray r5"><div /></div>
        <div className="icon yellow r3"><div /></div>
        <div className="icon purple r2"><div /></div>
        <div className="icon red r4"><div /></div>
        <div className="icon red-dark r1"><div /></div>
        <div className="icon gray r5"><div /></div>
    </div>
}
