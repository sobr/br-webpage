import {Header} from "./sections/Header";
import "./Page.sass";
import {About} from "./sections/About";
import {RoadMap} from "./sections/RoadMap";
import {Icons} from "./sections/Icons";
import {Faq} from "./sections/Faq";
import {Team} from "./sections/Team";
import {Discord} from "./sections/Discord";
import {Footer} from "./sections/Footer";
import {useEffect, useRef, useState} from "react";
import {Worldtour} from "./sections/Worldtour";
import {OutlookShort} from "./sections/OutlookShort";

export type Menu = "about"|"roadmap"|"worldtour"|"faq"|"team";

export function Page() {
    const [isMobile, setIsMobile] = useState<boolean>(true);
    const [currentMenu, setCurrentMenu] = useState<Menu>("about");

    const aboutRef = useRef<HTMLDivElement>(null);
    const worldtourRef = useRef<HTMLDivElement>(null);
    const roadmapRef = useRef<HTMLDivElement>(null);
    const faqRef = useRef<HTMLDivElement>(null);
    const teamRef = useRef<HTMLDivElement>(null);

    const refs = {
        "about": aboutRef,
        "worldtour": worldtourRef,
        "roadmap": roadmapRef,
        "faq": faqRef,
        "team": teamRef
    }

    const navigateTo = (menu: Menu) => {
        const ref = refs[menu];
        if (!ref || !ref.current) return;
        setCurrentMenu(menu);
        window.scrollTo({
            top: ref.current.offsetTop,
            left: 0,
            behavior: 'smooth'
        });
    };

    const setVh = () => {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    };

    useEffect(()=>{
        if (isMobile && document.body.offsetWidth>=480) {
            setIsMobile(false);
            return;
        }
        setVh();
        window.addEventListener("resize", ()=>setVh());

        return ()=>{

        };
    }, []);

    return <div className="sections">
        <div className="section">
            <Header onNavigation={menu => navigateTo(menu)}
                    activeNavigation={currentMenu}
                    isMobile={isMobile}
            />
        </div>
        <div className={"section " + (isMobile?"auto":"")} ref={aboutRef}>
            <About/>
        </div>
        <div className="section film-separator"/>
        <div className="section auto" ref={roadmapRef}>
            <RoadMap/>
        </div>
        <div className="section auto">
            <OutlookShort/>
        </div>
        <div className="section auto worldtour-section" ref={worldtourRef}>
            <Worldtour isMobile={isMobile}/>
        </div>
        <div className="section auto">
            <Icons/>
        </div>
        <div className="section auto" ref={faqRef}>
            <Faq/>
        </div>
        <div className="section auto" ref={teamRef}>
            <Team/>
        </div>
        <div className="section auto">
            <Discord/>
        </div>
        <div className="section auto">
            <Footer/>
        </div>
    </div>;
}
